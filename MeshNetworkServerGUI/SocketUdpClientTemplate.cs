﻿using MeshNetworkServer;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeshNetworkServerGUI
{
    namespace MeshNetworkServerClient
    {
        using System.Collections.Generic;

        /*
         * ПЕРЕД НАЧАЛОМ РАБОТЫ ВАЖНО ПРОЧИТАТЬ КОД И ВСЕ КОММЕНТАРИИ В ЭТОМ ФАЙЛЕ.
         * Можно сделать свою реализацию UDP узла, а можно воспользоватьэтим шаблоном.
         * Шаблон специально коряво написан, чтобы у всех вышел разный код,
         * ведь все будут по разному решать проблемы и баги.
         * Для тестирования работы вашего клиента можно запустить приложение и стартануть и сервер,
         * сервер пишет в логи все свои действия, там читайте, через них дебажте
         */

        internal class SocketUdpClientTemplate
        {
            // Тут вы должны придумать как вы будете хранить все соседние узлы
            // Лучше, если ввод параметров соседних узлов будет из интерфейса или консоли
            // Здесь для примера храниться только 1 сосед
            private static string[] remoteAddress; // адрес для отправки

            private static int remotePort = 8005; // порт для отправки
            private static int localPort = 8004; // порт для получения

            private static readonly Task taskReceive;
            private static Task taskSend;

            private static CancellationTokenSource cancellationTokenSource;

            private static CancellationTokenSource tokenSource;

            private static CancellationToken tokenSend;

            private static Thread listenThread;

            private static UdpClient receiverClient;

            private static Queue<Package> packages;

            private static uint[] idArray;

            private static bool stopReceive;

            public static void StartClient()
            {
                try
                {
                    stopReceive = false;
                    idArray = new uint[255];
                    for (int i = 0; i < 255; i++)
                    {
                        idArray[i] = 0;
                    }
                    packages = new Queue<Package>();
                    tokenSource = new CancellationTokenSource();
                    tokenSend = tokenSource.Token;
                    cancellationTokenSource = new CancellationTokenSource();

                    remotePort = MainWindow.connectionParameters.PortSend;
                    localPort = MainWindow.connectionParameters.PortReceive;

                    remoteAddress = MainWindow.connectionParameters.DestinationIp;
                    listenThread = new Thread(ReceiveFunc)
                    {
                        Name = "Receive packets from others"
                    };

                    listenThread.Start();
                    taskSend = new Task(() => SendMessage(tokenSend));
                    taskSend.Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            private static void ReceiveFunc()
            {
                try
                {
                    try
                    {
                        receiverClient = new UdpClient(localPort);
                    }
                    catch (System.Net.Sockets.SocketException e)
                    {
                        SocketError excp = e.SocketErrorCode;
                        if (excp == SocketError.AddressAlreadyInUse)
                        {
                        }
                        else
                        {
                            throw;
                        }
                    }

                    IPEndPoint remotEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.10"), localPort);

                    byte[] dataBytes = new byte[MeshNetworkServer.Package.bufferSize];
                    while (!stopReceive)
                    {
                        dataBytes = receiverClient.Receive(ref remotEndPoint);
                        //TODO make it shut down
                        Package package = Package.FromBinary(dataBytes);
                        lock (packages)
                        {
                            packages.Enqueue(package);
                        }
                    }

                    // SocketShutdown socketShutdown;

                    receiverClient.Client.Shutdown(SocketShutdown.Receive);
                    receiverClient.Client.Close();
                    receiverClient.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            private static void SendMessage(CancellationToken token)
            {
                UdpClient sender = new UdpClient();
                byte[] data = new byte[Package.bufferSize];
                try
                {
                    while (true)
                    {
                        if (token.IsCancellationRequested)
                        {
                            return;
                        }

                        Package pack = new Package(MeshNetworkServerGUI.MainWindow.OurDataStructure);

                        pack.ToBinary(data);
                        foreach (string addr in remoteAddress)
                        {
                            sender.Send(data, data.Length, addr, remotePort);
                        }
                        lock (packages)
                        {
                            if (packages.Count > 0)
                            {
                                for (int i = 0; i < 5; i++)
                                {
                                    packages.Dequeue().ToBinary(data);

                                    foreach (string addr in remoteAddress)
                                    {
                                        sender.Send(data, data.Length, addr, remotePort);
                                    }

                                    if (packages.Count == 0)
                                    {
                                        break;
                                    }
                                }
                            }
                        }

                        Thread.Sleep(2000);//задержка между сообщениями
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
                finally
                {
                    sender.Close();
                }
            }

            private static Package GenerateData()
            {
                Package pack = new Package
                {
                    //Здесь вы генирируете пакеты с реалистичными рандомными значениями
                    //Не просто рандом, а хотябы в реалистичных границах
                    //Для понимания смотрите файл Package.cs
                    PackageId = 11,        // Для примера
                    NodeId = 1,            // Для примера
                    Time = DateTime.Now,   // Для примера
                    Humidity = 11,         // Для примера
                    IsFire = false,        // Для примера
                    Lighting = 11,         // Для примера
                    Pressure = 11,         // Для примера
                    Temperature = 11      // Для примера
                };
                //
                return pack;
            }

            //private static void ReceiveMessage(CancellationToken token)
            //{
            //    UdpClient receiver = new UdpClient(localPort);
            //    IPEndPoint remoteIp = null; // адрес входящего подключения
            //    try
            //    {
            //        while (true)
            //        {
            //            if (CancellationTokenSource.IsCancellationRequested)
            //            {
            //                receiver.Close();
            //                //  taskReceive.Dispose();
            //                return;
            //            }

            //            byte[] data = receiver.Receive(ref remoteIp); // входящий пакет байт
            //            Package pack = Package.FromBinary(data); //преобразование в пакет
            //                                                     /*
            //                                                      * Здесь нужно проверить id пакета (как? - смотри файл Package.cs и думай)
            //                                                      * И если пакет с таким id ранее не был получен, то:
            //                                                      *     - отправить всем соседям
            //                                                      *      (хорошо бы использовать широкофещательную рассылку: https://metanit.com/sharp/net/5.3.php)
            //                                                      *     - сохранить его id в список или перезаписываемый массив
            //                                                      *      (достаточно хранить 255 последних пакетов)
            //                                                      * Иначе забыть про этот пакет
            //                                                      */
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        MessageBox.Show(exception.Message);
            //    }
            //    finally
            //    {
            //        receiver.Close();
            //    }
            //}

            public static void ClientStop()
            {
                // Эту функцию тоже желательно не так коряво реализовать,
                // она на данный момент вообще не всего клиента завершает
                // receiveThread.Abort();

                cancellationTokenSource.Cancel();
                //   listenThread.Abort();
                tokenSource.Cancel();
                //  taskReceive.Wait();
                // listenThread.Abort();
                //  receiverClient.Close();
                stopReceive = true;
            }
        }
    }
}